#pragma once

#include <SSTR.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <stdexcept>
#include <string>

/*
A templated message queue handler.

WARNING: using this class with a T of dynamic memory size will cause segfaults, as it is unclear how much shared
memory should be reserved, written, or read.
For this reason, this class should not be used with std::string, or any variable-size collection type

Destruction of the backing message queue needs to be handled externally.
Automatic destructor behaviour is to clean up local resources, but not the queue itself.

Shutdown(true) only needs to be called once per queue. Doing so will cause all other QueueHandlers using this queue to
become invalid.
*/
template <class T>
class QueueHandler {
 public:
  inline QueueHandler();
  inline ~QueueHandler();

  /*
  Pre: <file> points to an existing file where user has at least rw- rights
  Post: reader is now initialized. Read() and Write() can now be called

  If <createFile> is true, the file is touched before opening the queue.
  */
  inline void Init(const std::string& file, bool createFile = true);

  /*
  Pre: QueueHandler was initialized.
  Post: <message> is now copied into the queue, and the first connected QueueHandler to call Read() will receive it.

  <type> refers to the message priority.
  QueueHandlers can filter which types they would like to receive in a Read() call.
  lower values have a higher priority.
  <type> cannot be 0 or negative

  Throws:
    - std::invalid_argument if type was 0 or negative
    - std::runtime_error if the handler was not initialized
    - std::runtime_error if the message could not be written
  */
  inline void Write(T* message, long type = 1);

  /*
  Pre: QueueHandler was initialized
  Post: The contents of a message are copied to <output>

  <type> specifies how messages should be filtered.
  It is based on <type> specified in QueueHandler::Write(T*, <type>)
     0 : read first available message in chronological order
    >0 : read first message where type == <type>
    <0 : read first message where type <= abs(<type>)

  For example: if three messages are present in the queue, with types: 3, 1, 2,
  then Read(T*, 0) would return the first message (type 3)
  Read(T*, -2) would return the second message (type 1 is lower or equal than 2)
  Read(T*, 2) would return the third message (type 2)
  */
  inline void Read(T* output, long type = 0);

  /*
  Pre: -
  Post: QueueHandler is now shut down.
    If <destroy> is true, then the entire message queue is shut down, invalidating all other QueueHandlers on it.

  Can safely be called multiple times.
  Is automatically called in the destructor.
*/
  inline void Shutdown(bool destroy = false);

 private:
  std::string fileName;
  int msqid;
  key_t key;
  bool valid;
  size_t bufferSize;

 private:
  const char KEY_TOKEN = 'X';

 private:
  typedef struct TBuffer {
    long msgType;
    uint8_t payload[sizeof(T)];
  } TBuffer_t;
};

//====================================================================
// Function definitions start here
// All are declared inline to allow for late compilation
// If functions were compiled now, and a client would use it with T == <SomeCustomClientType>,
// compilation would fail, as QueueHandler<SomeCustomClientType> was never compiled before linking
//====================================================================

template <class T>
inline QueueHandler<T>::QueueHandler() : valid(false) {
  bufferSize = sizeof(TBuffer_t);
}

template <class T>
inline QueueHandler<T>::~QueueHandler() {
  Shutdown();
}

template <class T>
void QueueHandler<T>::Init(const std::string& file, bool createFile /* = true*/) {
  if (file.empty()) {
    throw std::invalid_argument("Empty string as queue filename");
  }

  Shutdown();

  if (createFile) {
    if (system(SSTR("touch " << file).c_str()) == -1) {
      std::string error = SSTR("failed to touch file: " << strerror(errno));
      Shutdown();
      throw std::runtime_error(error);
    }
  }

  if ((key = ftok(file.c_str(), KEY_TOKEN)) == -1) {
    std::string error = SSTR("failed to generate key: " << strerror(errno));
    Shutdown();
    throw std::runtime_error(error);
  }

  if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1) {
    std::string error = SSTR("failed to open or create queue: " << strerror(errno));
    Shutdown();
    throw std::runtime_error(error);
  }

  fileName = file;
  valid = true;
}

template <class T>
inline void QueueHandler<T>::Shutdown(bool destroy /* = false */) {
  if (destroy && valid) {
    msgctl(msqid, IPC_RMID, NULL);
    if (system(SSTR("rm " << fileName).c_str()) == -1) {
      std::cout << "Unable to remove file " << fileName;
    }
  }
  valid = false;
  fileName = "";
  msqid = -1;
}

template <class T>
inline void QueueHandler<T>::Read(T* output, long type) {
  if (!valid) {
    throw std::runtime_error("Not initialized");
  }

  TBuffer_t cache;
  if (msgrcv(msqid, &cache, bufferSize, type, 0) == -1) {
    std::string error = SSTR("Unable to read message: " << strerror(errno));
    throw std::runtime_error(error);
  }
  memcpy(output, &cache.payload[0], sizeof(T));
}

template <class T>
inline void QueueHandler<T>::Write(T* message, long type) {
  if (!valid) {
    throw std::runtime_error("Not initialized");
  }
  if (type <= 0) {
    throw std::invalid_argument("type can't be 0 or less");
  }

  TBuffer_t buffer;
  buffer.msgType = type;
  memcpy(&buffer.payload[0], message, sizeof(T));

  if (msgsnd(msqid, &buffer, bufferSize, 0) == -1) {
    std::string error = SSTR("Unable to write message: " << strerror(errno));
    throw std::runtime_error(error);
  }
}
