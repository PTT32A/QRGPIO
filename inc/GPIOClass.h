
#ifndef GPIO_CLASS_H
#define GPIO_CLASS_H

#include <string>

/* GPIO Class
 * Purpose: Each object instatiated from this class will control a GPIO pin
 * The GPIO pin number must be passed to the overloaded class constructor
 */
class GPIOClass
{
public:
	GPIOClass();
	GPIOClass(std::string gnum);
	int export_gpio();
	int unexport_gpio();
    int setdir_gpio(std::string dir);
    int setval_gpio(std::string val);
    int getval_gpio(std::string& val);
    std::string get_gpionum();
private:
	std::string gpionum;
};

#endif
