#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "GPIOClass.h"

void sig_handler(int sig);
void user_sig_handler(int sig);

bool ctrl_c_pressed = false;

GPIOClass* gpio4;
GPIOClass* gpio17;

int main (void)
{
	gpio4 = new GPIOClass("4");
	gpio17 = new GPIOClass("17");

	signal(SIGINT, sig_handler);
	signal(SIGUSR1, user_sig_handler);

	std::string inputstate;

    gpio4->export_gpio();
    gpio17->export_gpio();

    std::cout << " GPIO pins exported" << std::endl;

    gpio17->setdir_gpio("in");
    gpio4->setdir_gpio("out");

    std::cout << " Set GPIO pin directions" << std::endl;

    while(1)
    {
		gpio17->getval_gpio(inputstate);
				if(inputstate == "1")
				{
					raise(SIGUSR1);
				}

         if(ctrl_c_pressed)
                    {
                        std::cout << "Ctrl^C Pressed" << std::endl;
                        std::cout << "unexporting pins" << std::endl;
                        gpio4->unexport_gpio();
                        gpio17->unexport_gpio();
                        std::cout << "deallocating GPIO Objects" << std::endl;
                        delete gpio4;
                        gpio4 = 0;
                        delete gpio17;
                        gpio17 =0;
                        break;
                    }

    }
    std::cout << "Exiting....." << std::endl;
    return 0;
}

void sig_handler(int sig)
{
    printf("Caught signal ctrl c %d\n",sig);
    ctrl_c_pressed = true;
}

void user_sig_handler(int sig)
{
    //system("pgrep kill nameoffile") // close program
    std::cout << "resetting program "  << std::endl;
    gpio4->setval_gpio("1");
    usleep(500000);
    gpio4->setval_gpio("0");
    usleep(500000);
	//system("./nameoffile&"); // Start program
}
